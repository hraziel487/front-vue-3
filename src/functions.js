import Swal from "sweetalert2"; // Importa la librería SweetAlert2 para mostrar alertas personalizadas.
import axios from "axios"; // Importa la librería Axios para realizar solicitudes HTTP.

// Esta función muestra una alerta personalizada utilizando SweetAlert2.
export function showAlert(title, icono, foco = '') {
  if (foco !== '') {
    document.getElementById(foco).focus(); // Si se proporciona un elemento de enfoque, establece el enfoque en ese elemento.
  }

  Swal.fire({
    title: title, // Título de la alerta.
    icon: icono, // Icono de la alerta (puede ser 'success', 'error', 'info', etc.).
    customClass: { confirmButton: 'btn btn-primary', popup: 'animated zoomIn' }, // Clases CSS personalizadas.
    buttonsStyling: false, // Desactiva el estilo de los botones de SweetAlert2.
  });
}

// Esta función muestra una confirmación utilizando SweetAlert2 y realiza una acción en función de la respuesta.
export function confirmar(urlWithSlash, id, title, message) {
  var url = urlWithSlash + id; // Genera la URL para la solicitud.
  const swalWithBootstrapButton = Swal.mixin({
    customClass: { confirmButton: 'btn btn-success me-3', cancelButton: 'btn btn-danger' }, // Clases CSS personalizadas.
  });

  swalWithBootstrapButton.fire({
    title: title, // Título de la confirmación.
    text: message, // Mensaje de la confirmación.
    icon: 'question', // Icono de pregunta.
    showCancelButton: true, // Muestra el botón de cancelar.
    confirmButtonText: '<i class="fa-solid fa-check"></i> Yes, delete', // Texto del botón de confirmación.
    cancelButtonText: '<i class="fa-solid fa-ban"></i> Cancel', // Texto del botón de cancelar.
  }).then((res) => {
    if (res.isConfirmed) { // Si se confirma la acción.
      sendRequest('DELETE', { id: id }, url, 'Success Delete'); // Realiza una solicitud DELETE.
    } else { // Si se cancela la acción.
      showAlert('Operation Canceled', 'info'); // Muestra una alerta informativa.
    }
  });
}

// Esta función realiza una solicitud HTTP utilizando Axios y muestra una alerta en función del resultado.
export function sendRequest(method, parameters, url, message) {
  axios({ method: method, url: url, data: parameters }).then(function (res) {
    var status = res.status;
    if (status == 200) { // Si la solicitud es exitosa (código de estado 200).
      showAlert(message, 'success'); // Muestra una alerta de éxito.
      window.setTimeout(function () {
        window.location.href = '/'; // Redirige a la página principal después de 1 segundo.
      }, 1000);
    }
  });
}
